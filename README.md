## MiniStringFuck interpreter

Create an interpreter for the esoteric programming language MiniStringFuck.

See the specification:

https://esolangs.org/wiki/MiniStringFuck

See the tests.py file for two sample programs.